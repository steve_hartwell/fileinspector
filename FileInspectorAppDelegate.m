//
//  FileInspectorAppDelegate.m
//  FileInspector
//
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

// This delegate is instantiated in the main xib file.
// It doesn't need a "willFinishLaunching" method,
// but it does handle the -openDocument: menu action
// in the responder chain.

#import <Cocoa/Cocoa.h>

#import "FileInspectorDocument.h"

@interface SHHFileInspectorAppDelegate: NSObject<NSApplicationDelegate>

@end

@implementation SHHFileInspectorAppDelegate

- (void)openDocument: (id)sender
// override
// Replace the standard -[NSDocumentController openDocument:] method
// because our "file type" is any directory, which NSDocumentController
// is ill-prepared to handle.
{
    NSOpenPanel* openPanel = [NSOpenPanel openPanel];
    openPanel.canChooseDirectories = YES;
    openPanel.canChooseFiles = NO;
    openPanel.allowsMultipleSelection = YES;
    // openPanel.resolvesAliases = NO; // YES?
    
    [openPanel beginWithCompletionHandler:
        ^(NSInteger result) {
            if (result != NSFileHandlingPanelOKButton) {
                return;
            }
            NSDocumentController* docManager =
                [NSDocumentController sharedDocumentController];
            for (NSURL* aURL in openPanel.URLs) {
                [docManager openDocumentWithContentsOfURL:aURL
                                                  display:YES
                                        completionHandler:nil];
            }
        }];
}

@end
