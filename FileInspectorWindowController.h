//
//  FileInspectorWindowController.h
//  FileInspector
//
//  Created by Steve Hartwell on 8/28/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

// The window controller is responsible for laying out the window contents,
// including the toolbar and the content view, which includes a splitter
// and a status bar.
// It tries to stay away from knowing the contents of the subviews.

#import <Cocoa/Cocoa.h>

@interface SHHFileInspectorWindowController: NSWindowController

@end
