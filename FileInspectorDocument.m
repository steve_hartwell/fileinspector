//
//  FileInspectorDocument.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/28/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import "FileInspectorDocument.h"
#import "FileModel.h"
#import "FileInspectorWindowController.h"

@interface NSURL (SHHExtensions)
// work around a quirk in the way symlinks are resolved
- (NSURL*)URLByResolvingSymlinksInPathEvenInSlashPrivate;
@end


#pragma mark -

@interface SHHFileInspectorDocument ()
{
    NSManagedObjectModel* _managedObjectModel;
    NSManagedObjectContext* _managedObjectContext;
}
@property (nonatomic, assign) NSUInteger options;
@property (nonatomic, readwrite) BOOL loadingFiles;

@end

@implementation SHHFileInspectorDocument

- (instancetype)init
// override
// Called both when constructing a new Untitled document,
// and when opening an existing document.
{
    self = [super init];
    if (self) {
        self.options =
            NSDirectoryEnumerationSkipsHiddenFiles
            | NSDirectoryEnumerationSkipsSubdirectoryDescendants
            | NSDirectoryEnumerationSkipsPackageDescendants;
        self.loadingFiles = NO;
    }
    return self;
}

- (instancetype)initWithContentsOfURL: (NSURL*)url
                               ofType: (NSString*)typeName
                                error: (NSError**)outError
// override
// Called by NSDocumentController
{
    self = [self init];
    if (self) {
        self.fileURL = url;
    }
    return self;
}


#pragma mark -
#pragma mark Enumeration options

- (void)setIncludeHiddenItems: (BOOL)includeHiddenItems
{
    [self setOption:NSDirectoryEnumerationSkipsHiddenFiles
             enable:( ! includeHiddenItems)];
}
- (void)setRecurseSubdirectories: (BOOL)recurseSubdirectories
{
    [self setOption:NSDirectoryEnumerationSkipsSubdirectoryDescendants
             enable:( ! recurseSubdirectories)];
}
- (void)setRecursePackages: (BOOL)recursePackages
{
    [self setOption:NSDirectoryEnumerationSkipsPackageDescendants
             enable:( ! recursePackages)];
}
- (void)setOption: (NSDirectoryEnumerationOptions)option
           enable: (BOOL)enable
// bottleneck method to handle bit twiddling and kick off reloadData as needed.
{
    NSUInteger currentOptions = self.options;

    if (enable) {
        currentOptions |= option;
    }
    else {
        currentOptions &= ~(option);
    }

    if (self.options != currentOptions) {
        self.options = currentOptions;

        if (self.fileURL != nil) {
            [self reloadData];
        }
    }
}

- (void)reloadData
{
    NSManagedObjectContext *privQmoc =
        [[NSManagedObjectContext alloc]
            initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [privQmoc setParentContext:self.managedObjectContext];

    self.loadingFiles = YES;

    [privQmoc performBlock:
    ^{
        NSError *error = nil;

        // First, remove all the FileModel instances from the dataset.
        //
        NSFetchRequest* fetchem = [SHHFileModel fetchRequest];
        NSBatchDeleteRequest* killem =
            [[NSBatchDeleteRequest alloc] initWithFetchRequest:fetchem];
        [privQmoc.persistentStoreCoordinator
            executeRequest:killem withContext:privQmoc error:&error];

        [privQmoc.parentContext performBlockAndWait:^{
            NSError *parentSaveError = nil;
            if ( ! [privQmoc.parentContext save:&parentSaveError]) {
                NSLog(@"Error saving context: %@\n%@",
                      [parentSaveError localizedDescription],
                      [parentSaveError userInfo]);
                // abort();
            }
        }];
    }];

    [privQmoc performBlock:
    ^{
        NSError *error = nil;

        // Now enumerate with the currently set enumeration options.

        // NSDirectoryEnumerator won't accept symlinks, and symlinks in /private
        // aren't resolved by -[NSURL URLByResolvingSymlinksInPath],
        // so we have to do it the hard way.
        NSURL* dirURL = [self.fileURL
                            URLByResolvingSymlinksInPathEvenInSlashPrivate];

        // In addition to enumerating the file URLs themselves,
        // we ask NSDirectoryEnumerator to pick up the file metadata
        // such as file size, modification date, etc, and cache it
        // in the NSURL instance.
        // The SHHFileModel has a class property of the mapping between its
        // data members and the keys that NSURL uses to store this metadata.
        // So while constructing the enumerator, we pass the array of those
        // NSURL metadata keys for it to collect for us.
        NSDirectoryEnumerator *dirEnum =
            [[NSFileManager defaultManager]
                enumeratorAtURL:dirURL
                includingPropertiesForKeys:[SHHFileModel.attributeMap allValues]
                options:self.options
                errorHandler:
                    ^(NSURL* url, NSError* e)
                    {
                        return YES;
                    }];
        for (NSURL* aURL in dirEnum) {
            NSNumber *isDirectory;
            [aURL getResourceValue:&isDirectory
                            forKey:NSURLIsDirectoryKey
                             error:NULL];
            if ( ! [isDirectory boolValue]) {
                [SHHFileModel modelInManagedObjectContext:privQmoc withURL:aURL];
            }
        }

        if ( ! [privQmoc save:&error]) {
            NSLog(@"Error saving context: %@\n%@",
                  [error localizedDescription], [error userInfo]);
            // abort();
        }

        [privQmoc.parentContext performBlockAndWait:^{
            NSError *parentSaveError = nil;
            if ( ! [privQmoc.parentContext save:&parentSaveError]) {
                NSLog(@"Error saving context: %@\n%@",
                      [parentSaveError localizedDescription],
                      [parentSaveError userInfo]);
                // abort();
            }
            [self updateChangeCount:NSChangeCleared];
            self.loadingFiles = NO;
        }];
    }];
}

#pragma mark -
#pragma mark NSPersistentDocument

- (id)managedObjectModel
// override
// Not strictly necessary, but the default implementation gathers all of the
// data models it finds in the app bundle.  Convenient, but sloppy.
// This returns just the one model this code knows about.
{
    if (_managedObjectModel == nil) {

        NSString* modelPath =
            [[NSBundle bundleForClass:[self class]]
                pathForResource:@"FileInspectionModel" ofType:@"momd"];
        NSAssert(modelPath != nil, @"FileInspectionModel.momd not found");

        _managedObjectModel =
            [[NSManagedObjectModel alloc]
                initWithContentsOfURL:[NSURL fileURLWithPath:modelPath]];
        NSAssert(_managedObjectModel != nil, @"NSManagedObjectModel");
    }

    return _managedObjectModel;
}

- (NSManagedObjectContext*)managedObjectContext
// override
// Drat! All this code just so I can create the context
// with the NSMainQueueConcurrencyType, which it has to be
// because it updates NSArrayController and NSTableView instances.
// Or did I miss a memo?
{
    if (_managedObjectContext == nil) {

        _managedObjectContext =
            [[NSManagedObjectContext alloc]
                initWithConcurrencyType:NSMainQueueConcurrencyType];

        NSError* error = nil;
        NSPersistentStoreCoordinator *psc =
            [[NSPersistentStoreCoordinator alloc]
                initWithManagedObjectModel:self.managedObjectModel];
        [psc
            addPersistentStoreWithType:
                [self persistentStoreTypeForFileType:self.fileType]
            configuration:nil
            URL:[NSURL fileURLWithFileSystemRepresentation:"/tmp/qq.db"
                                               isDirectory:NO
                                             relativeToURL:nil]
            options:nil
            error:&error];

        if (error) {
            // TODO: deal with nonnil error
        }
        
        _managedObjectContext.persistentStoreCoordinator = psc;
        
        // no undo overhead needed, suppress.
        _managedObjectContext.undoManager = nil;
        
        [super setManagedObjectContext:_managedObjectContext];
    }
    
    return _managedObjectContext;
}

#if DEBUG
- (void)setManagedObjectContext:(NSManagedObjectContext*)managedObjectContext
// override
{
    if (managedObjectContext == nil) {
        [super setManagedObjectContext:managedObjectContext];
    }
    else {
        NSLog(@"%s: IGNORING %@", __func__, managedObjectContext);
    }
}
#endif // DEBUG

- (NSString*)persistentStoreTypeForFileType: (NSString*)fileType
// override
{
    return NSSQLiteStoreType;
}

#pragma mark -
#pragma mark NSDocument

- (void)makeWindowControllers
// override
// The UI aspect of an NSDocument instance: make some windows for your data.
{
    [self addWindowController:[SHHFileInspectorWindowController new]];
}

- (void)setFileURL: (NSURL*)dirURL
// override
{
    [super setFileURL:dirURL];
    
    if (dirURL == nil) {
        return;
    }

    [self reloadData];
}

- (NSUndoManager*)undoManager
// override
// No need for undo in our data model
{
    return nil;
}

@end

// -------------------------------------------------------------------------------


#pragma mark -

@implementation NSURL (SHHExtensions)

- (NSURL*)URLByResolvingSymlinksInPathEvenInSlashPrivate
// -[NSURL URLByResolvingSymlinksInPath] won't resolve symlinks in /private,
// so we have to do it the hard way.
{
    NSURL* resolvedURL = [self URLByResolvingSymlinksInPath];

    NSString* path = [resolvedURL path];
    NSError* err = nil;
    NSDictionary* pathAttributes =
        [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&err];

    if ( ! [pathAttributes[NSFileType] isEqualToString:NSFileTypeSymbolicLink]) {
        return resolvedURL;
    }

    return [[self class] fileURLWithPath:
                [[path stringByDeletingLastPathComponent]
                    stringByAppendingPathComponent:
                        [[NSFileManager defaultManager]
                            destinationOfSymbolicLinkAtPath:path
                            error:&err]]];
}

@end
