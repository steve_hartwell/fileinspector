//
//  FileModel.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/29/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

// This class is a lightweight facade for an NSManagedObject,
// which is the basic object type of Core Data.
//

// That URL is loaded up with all kinds of metadata gathered while
// the directory is being enumerated.
// So this class just extracts them as needed to provide the values.

#import "FileModel.h"


@implementation SHHFileModel


+ (instancetype)modelInManagedObjectContext: (NSManagedObjectContext*)context
                                    withURL: (NSURL*)fileURL
// convenience method
// Construct a file model with its all-important URL loaded with attributes.
// The instance will be retained by the managed object context.
{
    NSEntityDescription* entity = [self entityInContext:context];

    SHHFileModel* model =
        [[self alloc] initWithEntity:entity
      insertIntoManagedObjectContext:context];

    NSDictionary* attrMap = self.class.attributeMap;
    for (NSString* key in attrMap) {
        id value = nil;
        [fileURL getResourceValue:&value forKey:attrMap[key] error:NULL];
        [model setPrimitiveValue:value forKey:key];
    }
    [model setPrimitiveValue:[fileURL path] forKey:@"filepath"];
    [model setPrimitiveValue:fileURL forKey:@"fileurl"];

    return model;
}

+ (NSEntityDescription*)entityInContext: (NSManagedObjectContext*)context
{
    return [NSEntityDescription entityForName:@"FileEntity"
                       inManagedObjectContext:context];
}

+ (NSDictionary*)attributeMap
{
    static NSDictionary* _attributeMap = nil;
    if (_attributeMap == nil) {
        _attributeMap = @{
            @"filename":        NSURLLocalizedNameKey,
            @"fileicon":        NSURLEffectiveIconKey,
            @"filekind":        NSURLLocalizedTypeDescriptionKey,
            @"filesize":        NSURLTotalFileSizeKey, // not NSURLFileSizeKey
            @"filecreated":     NSURLCreationDateKey,
            @"filecontentacc":  NSURLContentAccessDateKey,
            @"filecontentmod":  NSURLContentModificationDateKey,
            @"fileattrmod":     NSURLAttributeModificationDateKey,
            @"isFolder":        NSURLIsDirectoryKey,
            @"isPackage":       NSURLIsPackageKey,
        };
    }
    return _attributeMap;
}

@end
