//
//  FileInfoViewController.h
//  FileInspector
//
//  Created by Steve Hartwell on 9/8/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

// This view controller depends mostly on its nib for the bindings
// from the selected item to its detail fields.

#import <Cocoa/Cocoa.h>

@interface SHHFileInfoViewController: NSViewController

@end
