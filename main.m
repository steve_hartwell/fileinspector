//
//  main.m
//  FileInspector
//
//  Created by Steve Hartwell on 8/29/15.
//  Copyright (c) 2015 Steve Hartwell. All rights reserved.
//

#import <AppKit/AppKit.h>

int main(int argc, const char* argv[])
{
    return NSApplicationMain(argc, argv);
}
